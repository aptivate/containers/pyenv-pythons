IMAGE="aptivate/pyenv-pythons"

docker-build:
	@docker build -t $(IMAGE) .
.PHONY: docker-build

docker-push:
	@docker push $(IMAGE)
.PHONY: docker-push
