[![pipeline status](https://git.coop/aptivate/containers/pyenv-pythons/badges/master/pipeline.svg)](https://git.coop/aptivate/containers/pyenv-pythons/commits/master)

# pyenv-pythons

A container with multiple Pythons installed.

Useful for testing with https://github.com/samstav/tox-pyenv.

## Add a Python

Add it to `python-versions.txt` and run:

```bash
$ make docker-build
$ docker login  # credentials in pass store
$ make docker-push
```
